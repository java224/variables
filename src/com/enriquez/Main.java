package com.enriquez; // the package where the Java class belong to

// The main entry point of our program
public class Main {
    // Access Modifier --> public
    // Return Type --> void
    // Pattern to create a function (method):
    /*
        accessModifier static returnType functionName(dataType arguments) {
            // code blocks
        }
    */

    public static void main(String[] args) {
	// write your code here
        System.out.println("** Variable **");

        //Variables

        /*
            Syntax:
            dataType identifier = value;
        */

        // Declare a variable without initialization
        int firstNum;

        // Declare a variable with initialization
        int secondNum = 23;

        // Initialize a variable after declaration
        firstNum = 34;

        //Variable reassignment
        secondNum = 24;

        // Constants

        /*
            Syntax
            final dataType identifier = value;
        */

        final float INTEREST = 0.02F; // F --> to tell java that this is float and not a double
    }
}
