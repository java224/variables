package com.enriquez;

import java.util.Scanner;

public class DataTypes {
    public static void main(String[] args) {
        System.out.println("** Data Types **");

        //Data Types

        // 1) Primitive (Built-in) --> it stores simple values

        byte age = 30;

        long worldPopulation = 7_862_081_145L; // L --> to tell Java that this is a long data type

        System.out.println("THe current word population is " + worldPopulation);

        float price = 12.99f; // F --> to tell Java this is float and not double

        char firstLetter = 'A'; // Use single quote to initialize a  character

        boolean isMarried = false;

        // 2) Non-Primitive (Referencing) --> it stores complex data (object)

        String completeName = "Isaac Enriquez"; // shorthand
        String completeName2 = new String("Isaac Joseph Enriquez"); // longhand

        // Mini-activity: Display to the console the current value of completeName.
        System.out.println(completeName);
        System.out.println(completeName2);

        completeName.toLowerCase(); // this invocation will return a string
        System.out.println(completeName.toLowerCase());
        String lowerCaseCompleteName = completeName.toLowerCase();
        System.out.println(lowerCaseCompleteName);

        // Escape Sequence
        System.out.println("C:\\Windows\\Desktop");
        System.out.println("I am a \"Software\" Developer");
        System.out.println("I am a \"Software Developer");

        // Arithmetic Expression
        int sum = 10 + 5;
        int product = 23 * 2;
        int diff = 34 - 30;
        double quotient = 100.0 / 25;
        System.out.println("10 + 5 = " + sum);
        System.out.println("23 * 2 = " + product);
        System.out.println("34 - 30 = " + diff);
        System.out.println("100.0 / 25 = " +quotient);

        //Casting (Type Casting)
        //Conversion of 1 data type to another

        // 1) Implicit (Automatic) Casting
        // byte > short > int > long > float > double
        //this will work in ascending order

        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("5 + 2.7 = " + total);

        // 2) Explicit Casting

        int num3 = 5;
        double num4 = 2.7;
        int total2 = num3 + (int) num4;
        System.out.println("5 + 2.7 = " + total2);

        // Converting things to integers
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println("Total grade is: " + (mathGrade + englishGrade));
        System.out.println("Total grade is: " + (Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade)));
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(Integer.toString(totalGrade));

        // Asking for input from a user
        Scanner appScanner = new Scanner(System.in);

        String firstName;
        int ageInput;

        System.out.println("What is your first name?");
        firstName = appScanner.nextLine().trim();
        System.out.println("Hello, " + firstName + "! Welcome to the world of Java Programming");

        // Mini-activity:
        // Ask for user's age and store it in ageInput
        // then display it (ageInput) to the console

        System.out.println("What is your age?");
        ageInput = appScanner.nextInt();
        System.out.println("So, " + firstName + ", you are " + ageInput + " years old?;");




    }
}
